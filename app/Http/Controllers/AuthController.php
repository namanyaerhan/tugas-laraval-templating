<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function welcome(Request $request){ //untuk varible request bisa disesuaikan sesuai keinginan kita
        // dd($request->all());
        $firstName = $request->firstname;
        $lastName = $request->lastname;

        return view('halaman.welcome',compact('firstName','lastName'));
    }
}
