@extends('layout.master')

@section('judul')
Detail Cast {{$cast->nama}}
@endsection

@section('content')
<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}} tahun</p>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-info">Kembali</a>
@endsection