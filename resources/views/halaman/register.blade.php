@extends('layout.master')

@section('judul')
Media Online
@endsection

@section('content')
<h1>Buat Account Baru</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
    @csrf
  <label for="firstname">First Name:</label><br />
  <input type="text" name="firstname" id="firstname" /><br />
  <label for="lastname">Last Name:</label><br />
  <input type="text" name="lastname" id="lastname" /><br /><br />

  <label for="gender">Gender:</label><br />
  <input type="radio" name="gender" />Male <br />
  <input type="radio" name="gender" />Female <br />
  <input type="radio" name="gender" />Other <br /><br />

  <label for="Nationality">Nationality:</label><br />
  <select>
    <option value="indonesia">Indonesia</option>
    <option value="inggris">Inggris</option>
  </select>
  <br /><br />

  <label for="language">Language Spoken:</label><br />
  <input type="checkbox" />Bahasa Indonesia <br />
  <input type="checkbox" />English <br />
  <input type="checkbox" />Other

  <br /><br />
  <label for="bio">Bio:</label><br />
  <textarea name="bio" cols="30" rows="10"></textarea> <br />
  <input type="submit" value="Sign Up" />
</form>
@endsection
